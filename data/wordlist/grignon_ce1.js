var data = {
    "title": "Listes de mots Grignon CE1 2023-2024",
    "lists": [
        {
            "title": "Liste 0",
            "words": ["longtemps", "avant", "pourquoi", "alors", "soudain", "enfin", "après", "tout à coup", "aujourd'hui"]
        },
        {
            "title": "Liste 08",
            "words": ["Les vacances", "Un enfant", "pendant", "souvent", "blanc", "un pantalon", "un éléphant", "un serpent", "manger", "quand", "maintenant", "novembre", "ensuite"]
        },
        {
            "title": "Liste 09",
            "words": ["une fleur", "un danseur", "la peur", "un oeuf", "ma soeur", "les yeux", "heureux", "un cheveu", "des oeufs", "joyeux", "un jeu", "le coeur", "la couleur"]
        },
        {
            "title": "Liste 10",
            "words": ["l'école","le boulanger", "le déjeuner", "rentrer", "chez", "une idée", "la journée", "le premier", "le nez", "un pied", "le dernier", "du papier", "assez"]
        },
        {
            "title": "Liste 11",
            "words": ["Mon grand-père", "il achète", "après", "une fenêtre", "un poulet", "la sorcière", "elle se promène", "la forêt", "la tête", "mon frère", "la fête", "être", "même", "un jouet"]
        },
        {
            "title": "Liste 12",
            "words": ["Une adresse", "La maitresse", "une lettre", "des lunettes", "la terre", "le ciel", "l'hiver", "chercher", "merci", "vert (la couleur)", "un anniversaire", "hier"]
        },
        {
            "title": "Liste 13",
            "words": ["Jamais", "une maison", "la neige", "la reine", "aimer", "le lait", "seize", "treize", "faire", "elle fait", "c'est vrai", "la semaine", "une baleine"]
        },
        {
            "title": "Liste 14",
            "words": ["un garçon", "gourmand", "une guitare", "une baguette", "le garage", "gris", "le guidon", "fatigué", "à gauche", "une gomme", "un légume", "la langue", "une guêpe"]
        }
    ]
}