class QuestionGenerator {
    // first and second are array of integers that will be used to compose questions.
    constructor(first, second) {
        this.first = first;
        this.second = second;
        this.currentFirst = first[0];
        this.currentSecond = second[0];
    }

    symbol() {
        // To implement in sub classes
    }
    // returns an array of 2 integers
    // TODO enhance by precomputing all possible pairs and iterate over them. So that the same question is not asked twice
    // TODO enhance previous todo by adding back (at the beginning of the iterator) answers that are not validated !!!!
    newQuestion() {
        this.currentFirst = this.nextFirst()
        this.currentSecond = this.nextSecond()
    }

    nextSecond() {
        return this.second[randomInteger(this.second.length)];
    }

    nextFirst() {
        return this.first[randomInteger(this.first.length)];
    }

    // toValidate is an integer to match against currentFirst and currentSecond
    // Returns a validation with a success boolean and a first operand, second operand, answer, real result
    validate(first, second, toValidate) {
        // to implement in sub classes
    }

    validateAnswer(toValidate) {
        return this.validate(this.currentFirst, this.currentSecond, toValidate)
    }
    // returns a message displaying the question
    message() {
        // to implement in subclasses
    }

    // kind of private function to get an arbitrary value in an array
    randomInteger(max) {
        return Math.floor(Math.random() * max);
    }
}

class MultGenerator extends QuestionGenerator {
    validateAnswer(first, second,toValidate) {
        let compute = first * second
        return { 
            success: compute === toValidate, 
            firstOperand: first, 
            secondOperand: second, 
            response: toValidate, 
            result: first * second 
        }
    }

    message() {
        return this.currentFirst + " x " + this.currentSecond + " = ???"
    }

    symbol() {
        return "x"
    }
}

class DivGenerator extends QuestionGenerator {
    validateAnswer(first, second, toValidate) {
        return { 
            success: second === toValidate, 
            firstOperand: first * second, 
            secondOperand: first, 
            response: toValidate, 
            result: second 
        }
    }

    message() {
        return (this.currentFirst * this.currentSecond ) + " / " + this.currentFirst + " = ???"
    }

    symbol() {
        return "/"
    }
}

class AdditionGenerator extends QuestionGenerator {
    validateAnswer(first, second, toValidate) {
        let compute = first + second
        return { 
            success: compute === toValidate, 
            firstOperand: first, 
            secondOperand: second, 
            response: toValidate, 
            result: first + second 
        }
    }

    message() {
        return this.currentFirst + " + " + this.currentSecond + " = ???"
    }

    symbol() {
        return "+"
    }

}
