function go() {
    document.getElementById("settings").style.display = "none"
    document.getElementById("compute").style.display = "block"
    activate()
}
function showSettings() {
    document.getElementById("settings").style.display = "block"
    document.getElementById("compute").style.display = "none"
}

function initSettings() {
    document.getElementById("compute").style.display='none'
    document.getElementById("go").addEventListener('click', go)
    document.getElementById("doSettings").addEventListener('click', showSettings)
    document.getElementById("doSettings").addEventListener('click', reset)
    reset()
}

window.addEventListener("DOMContentLoaded", initSettings)
