
function init() {
    // read datas
    processList(data)    
}

function toJson(response) {
    return response.toJson;
}

function processList(metaList) {
    // read name then process sublists
    console.log(metaList.title)
    document.getElementById("placeholder").innerHTML = buildDetails(metaList)
}

function buildDetails(metaList) {
    let result = `<details open><summary>${metaList.title}</summary>`
        metaList.lists.forEach(list => {
            result += `<details><summary>${list.title} <a href="wordList.html?title=${list.title}&values=${list.words.join(",")}">Lien</a>&nbsp;<a href="wordQuiz.html?title=${list.title}&values=${list.words.join(",")}">Quiz</a></summary><ul>`
            list.words.forEach(word => {
                result+=`<li>${word}</li>`
            })
            result+='</ul></details>'
        });
        result+="</details>"
    return result;
}

window.addEventListener("DOMContentLoaded", init)