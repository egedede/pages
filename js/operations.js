let multStatus = {
    "generator": new MultGenerator([1,2,3,4,5,6,7,8,9],[1,2,3,4,5,6,7,8,9]),
    "factor": -1,
    "multiplicand": -1,
    "results": [],
    isDiv: function () {
        return this.type === 'div';
    },
    nbSuccess: function () {
        let result = 0;
        for (let answer of this.results) {
            if (answer.success) {
                result++
            }
        }
        return result;
    },
    percentSuccess: function () {
        return this.nbSuccess() / this.results.length;
    }
}

function newQuestion() {
    multStatus.generator.newQuestion()
    document.getElementById("mult-question-label").textContent = multStatus.generator.message() 
    activate()
}

function randomInteger(max) {
    return Math.floor(Math.random() * max);
}

function toInteger(item) {
    return parseInt(item.innerText)
}

function submit() {
    let duration = ((performance.now() - start) / 1000).toFixed(2)
    const valueAsString = document.getElementById("mult-answer-input").value
    if (valueAsString) {
        let inputValue = parseInt(valueAsString)
        result = multStatus.generator.validateAnswer(multStatus.generator.currentFirst, multStatus.generator.currentSecond, inputValue)
        result.duration = duration
        multStatus.results.push(result)
        document.getElementById("mult-summary-details").innerHTML = buildAnswerTable(multStatus.results)
        document.getElementById("mult-summary-status").textContent = multStatus.nbSuccess() + "/" + multStatus.results.length
        color = multStatus.percentSuccess() * 255
        document.getElementById("mult-summary-status").setAttribute("style", `background-color:rgb(${255 - color},${color},0)`)
        newQuestion()
    }
}

function newGenerator() {
    let sFactors = document.getElementById("factors").querySelectorAll(".selected")
    let selectedFactors = Array.from(sFactors).map(toInteger)
    let selectedMultiplicand = Array.from(document.getElementById("multiplicands").querySelectorAll(".selected")).map(toInteger)
    let type = document.querySelector("#type .selected").getAttribute("id")
    switch(type) {
        case "mult" : 
            multStatus.generator = new MultGenerator(selectedFactors, selectedMultiplicand)
            break
        case "div" : 
            multStatus.generator = new DivGenerator(selectedFactors, selectedMultiplicand)
            break
        case "add" : 
            multStatus.generator = new AdditionGenerator(selectedFactors, selectedMultiplicand)
            break
    }

    newQuestion()
}

function buildAnswerTable(results) {
    let result = '<table class="center"><thead></thead><tbody>'
        results.slice().reverse().forEach(function (answer) {
            let status = answer.success ?
                '&#10004' :
                `${answer.result} &#128711;`
            result += `<tr><td>${answer.firstOperand}</td><td>${multStatus.generator.symbol()}</td><td>${answer.secondOperand}</td><td>=</td><td>${answer.response}</td><td>${status}</td><td>${answer.duration}</td>`
        })
    result += '</tbody></table>'
    return result
}
function reset() {
    multStatus.results = []
    document.getElementById("mult-summary-status").textContent = ''
    document.getElementById("mult-summary-details").textContent = ''
    newQuestion()
}
function init() {
    // prepare numbers by selecting them all and enabling the click on them
    document.querySelectorAll(".number").forEach((el) => {
        el.classList.add("selected")
        el.addEventListener("click", toggleSelection)
    })

    // allow keydown as a submit trigger
    document.getElementById("mult-answer").addEventListener("keydown", function (e) {
        if (e.keyCode === 13) {
            submit()
        }
    })
    // Enable the click on the button to submit answer
    document.getElementById("mult-answer-submit").addEventListener("click", submit)

    // init the switch between div and mult
    document.querySelectorAll("#type .squared").forEach((el) => {
        el.addEventListener("click", selectOneType)
    })
    // init data
    newQuestion()
}

function activate() {
    document.getElementById("mult-answer-input").focus()
    document.getElementById("mult-answer-input").value = ''
    start = performance.now()
}

function toggleSelection(item) {
    item.target.classList.toggle("selected")
    newGenerator()
}
function selectOneType(item) {
    document.querySelectorAll("#type .squared").forEach((el) => {
        el.classList.remove("selected")
    })
    item.target.classList.toggle("selected")
    multStatus.type = item.target.id
    newGenerator()
}



window.addEventListener("DOMContentLoaded", init)
