
function init() {
    // read datas
    const params = new Proxy(new URLSearchParams(window.location.search), {
        get: (searchParams, prop) => searchParams.get(prop),
      });
    document.getElementById("title").innerHTML = params.title;
    let result = "<ul>"; 
    params.values.split(",").forEach(item => {
        result+=`<li>${item}</li>`
    })      
    result+="</ul>"
    document.getElementById("placeholder").innerHTML = result;
}


window.addEventListener("DOMContentLoaded", init)