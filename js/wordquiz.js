let wordList;
function init() {
    // read datas
    const params = new Proxy(new URLSearchParams(window.location.search), {
        get: (searchParams, prop) => searchParams.get(prop),
      });
    document.getElementById("title").innerHTML = params.title;
    wordList = params.values.split(",").reverse();
    document.getElementById("currentValue").innerHTML = wordList.pop();
    document.getElementById("next").addEventListener("click", next);
    document.getElementById("card").addEventListener("click", next);
}

function next() {
    document.getElementById("currentValue").innerHTML = wordList.pop();
}


window.addEventListener("DOMContentLoaded", init)