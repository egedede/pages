let multStatus = {
    "type": "mult",
    "factor": -1,
    "multiplicand": -1,
    "results": [],
    isDiv: function () {
        return this.type === 'div';
    },
    nbSuccess: function () {
        let result = 0;
        if (this.isDiv()) {
            for (let answer of this.results) {
                if (answer.answer === answer.multiplicand) {
                    result++
                }
            }
        } else {
            for (let answer of this.results) {
                if (answer.factor * answer.multiplicand === answer.answer) {
                    result++
                }
            }
        }
        return result;
    },
    percentSuccess: function () {
        return this.nbSuccess() / this.results.length;
    }
}

function newQuestion() {
    let sFactors = document.getElementById("factors").querySelectorAll(".selected")
    let selectedFactors = Array.from(sFactors).map(toInteger)
    let selectedMultiplicand = Array.from(document.getElementById("multiplicands").querySelectorAll(".selected")).map(toInteger)
    let selectedFactorIndex = selectedFactors[randomInteger(selectedFactors.length)]
    let selectedMultiplicandIndex = selectedMultiplicand[randomInteger(selectedMultiplicand.length)]
    if (multStatus.isDiv()) {
        document.getElementById("mult-question-label").textContent = (selectedFactorIndex * selectedMultiplicandIndex) + " / " + selectedFactorIndex + " = ???"
    } else {
        document.getElementById("mult-question-label").textContent = selectedFactorIndex + " x " + selectedMultiplicandIndex + " = ???"
    }
    multStatus.factor = selectedFactorIndex
    multStatus.multiplicand = selectedMultiplicandIndex
    activate()
}

function randomInteger(max) {
    return Math.floor(Math.random() * max);
}

function toInteger(item) {
    return parseInt(item.innerText)
}

function submit() {
    let duration = ((performance.now() - start) / 1000).toFixed(2)
    const valueAsString = document.getElementById("mult-answer-input").value
    if (valueAsString) {
        let inputValue = parseInt(valueAsString)
        let success = multStatus.isDiv() ?
            multStatus.multiplicand === inputValue
            : multStatus.factor * multStatus.multiplicand === inputValue
        multStatus.results.push({ "factor": multStatus.factor, "multiplicand": multStatus.multiplicand, "answer": inputValue, "success": success, "duration": duration })
        document.getElementById("mult-summary-details").innerHTML = buildAnswerTable(multStatus.results)
        document.getElementById("mult-summary-status").textContent = multStatus.nbSuccess() + "/" + multStatus.results.length
        color = multStatus.percentSuccess() * 255
        document.getElementById("mult-summary-status").setAttribute("style", `background-color:rgb(${255 - color},${color},0)`)
        newQuestion()
    }
}

function buildAnswerTable(results) {
    let result = '<table class="center"><thead></thead><tbody>'
    if (multStatus.isDiv()) {
        results.slice().reverse().forEach(function (answer) {
            let status = answer.success ?
                '&#10004' :
                `${answer.multiplicand} &#128711;`
            result += `<tr><td>${answer.factor * answer.multiplicand}</td><td>${answer.factor}</td><td>${answer.answer}</td><td>${status}</td><td>${answer.duration}</td>`
        })
    } else {
        results.slice().reverse().forEach(function (answer) {
            let status = answer.success ?
                '&#10004' :
                `${answer.factor * answer.multiplicand} &#128711;`
            result += `<tr><td>${answer.factor}</td><td>${answer.multiplicand}</td><td>${answer.answer}</td><td>${status}</td><td>${answer.duration}</td>`
        })
    }
    result += '</tbody></table>'
    return result
}
function reset() {
    multStatus.results = []
    document.getElementById("mult-summary-status").textContent = ''
    document.getElementById("mult-summary-details").textContent = ''
    newQuestion()
}
function init() {
    // prepare numbers by selecting them all and enabling the click on them
    document.querySelectorAll(".number").forEach((el) => {
        el.classList.add("selected")
        el.addEventListener("click", toggleSelection)
    })

    // allow keydown as a submit trigger
    document.getElementById("mult-answer").addEventListener("keydown", function (e) {
        if (e.keyCode === 13) {
            submit()
        }
    })
    // Enable the click on the button to submit answer
    document.getElementById("mult-answer-submit").addEventListener("click", submit)

    // init the switch between div and mult
    document.querySelectorAll("#type .squared").forEach((el) => {
        el.addEventListener("click", selectOneType)
    })
    // init data
    newQuestion()
}

function activate() {
    document.getElementById("mult-answer-input").focus()
    document.getElementById("mult-answer-input").value = ''
    start = performance.now()
}

function toggleSelection(item) {
    item.target.classList.toggle("selected")
    newQuestion()
}
function selectOneType(item) {
    document.querySelectorAll("#type .squared").forEach((el) => {
        el.classList.remove("selected")
    })
    item.target.classList.toggle("selected")
    multStatus.type = item.target.id
    newQuestion()
}



window.addEventListener("DOMContentLoaded", init)
